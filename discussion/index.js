function printname(){
	console.log("My name is Alvin");
};

printname();

// Function declaration vs expression

function declaredFunction(){
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction")
}

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!");
};

constantFunc();

// Function scoping 

/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function shownames() {
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst)
	console.log(functionLet)
}

shownames()


// Nested Functions

function myNewFunction(){
	let name = "Jane";

	function nestedFunction() {
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction()
}

myNewFunction();

// Function and Global scoped variables 

// Global scoped variable 
 
let globalName = "Joy";

function myNewFunction2(){
	let nameInside = "Kenzo";

	console.log(nameInside)
	console.log(globalName)
}

myNewFunction2();


// Using alert()

alert("Hello, world!"); // This will run immediately when the page loads

function showSampleAlert(){
	alert("Hello, user!");
}

showSampleAlert();

// Using prompt()

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")
}

printWelcomeMessage();

/*
 Function naming conventions
	- Function names should be definitive of the task it will perform
	- Avoid generic names to avoid confusion within the code
	- Avoid pointless and inappropriate functions names
	- Name your functions following camel casing.
	- Do not use JS reserved keywords
*/
